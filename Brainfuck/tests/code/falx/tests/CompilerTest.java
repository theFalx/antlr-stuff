package code.falx.tests;

import code.falx.antlr.brainfuck.BrainfuckCompiler;
import code.falx.antlr.brainfuck.grammar.generated.BrainfuckLexer;
import code.falx.antlr.brainfuck.grammar.generated.BrainfuckParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.IOError;
import java.io.IOException;

/**
 * @author falx
 * @created 05.01.2015
 */
public class CompilerTest
{
  //<editor-fold desc="Fields">


  //</editor-fold>

  //<editor-fold desc="Constructor">
  public static void main ( String[] args )
          throws IOException
  {
    BrainfuckCompiler compiler = new BrainfuckCompiler( "F:\\repositories\\antlr-stuff\\Brainfuck\\tests\\code\\falx\\tests", "Test" );
    /*ANTLRInputStream input = new ANTLRInputStream( "+++[-]" );*/
    ANTLRInputStream input = new ANTLRInputStream( "+++[-]" );
    BrainfuckLexer lexer = new BrainfuckLexer( input );
    BrainfuckParser parser = new BrainfuckParser( new CommonTokenStream( lexer ) );

    compiler.visitProgram( parser.program() );
  }

  //</editor-fold>

  //<editor-fold desc="Methods">


  //</editor-fold>
}
