package code.falx.tests;

import code.falx.antlr.brainfuck.BrainfuckInterpreter;
import code.falx.antlr.brainfuck.grammar.generated.BrainfuckLexer;
import code.falx.antlr.brainfuck.grammar.generated.BrainfuckParser;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;

/**
 * @author falx
 * @created 03.01.2015
 */
public class SimpleTest
{
  //<editor-fold desc="Fields">


  //</editor-fold>

  //<editor-fold desc="Constructor">


  //</editor-fold>

  //<editor-fold desc="Methods">

  public static void main ( String[] args )
  {
    BrainfuckInterpreter interpreter = new BrainfuckInterpreter();
    ANTLRInputStream input = new ANTLRInputStream( "+++[-]" );
    BrainfuckLexer lexer = new BrainfuckLexer( input );
    BrainfuckParser parser = new BrainfuckParser( new CommonTokenStream( lexer ) );

    interpreter.visitProgram( parser.program() );
    int val = interpreter.getBrainfuckVM().getValue( 0 );
    System.out.println ( val );
  }

  //</editor-fold>
}
