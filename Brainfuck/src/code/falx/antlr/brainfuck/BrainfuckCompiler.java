package code.falx.antlr.brainfuck;

import code.falx.antlr.brainfuck.grammar.generated.BrainfuckBaseVisitor;
import code.falx.antlr.brainfuck.grammar.generated.BrainfuckParser.ExpressionContext;
import code.falx.antlr.brainfuck.grammar.generated.BrainfuckParser.LoopContext;
import code.falx.antlr.brainfuck.grammar.generated.BrainfuckParser.ProgramContext;
import org.antlr.v4.runtime.misc.NotNull;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author falx
 * @created 03.01.2015
 */
public class BrainfuckCompiler
        extends BrainfuckBaseVisitor<Object>
{
  //<editor-fold desc="Fields">

  private String filename;
  private boolean mCompile;
  private File mCompiledCodeFile;
  private int mCurPointer;
  private int mCurValue;
  private FileWriter mFileWriter;
  private File mJavacodeFile;
  private char mLastOp;

  //</editor-fold>

  //<editor-fold desc="Constructor">

  public BrainfuckCompiler ( String outputPath, String filename )
          throws IOException
  {
    this.mCompile = true;
    this.filename = filename;
    String path = this.buildPath( outputPath, filename );
    this.mJavacodeFile = new File( path + ".java" );
    this.mJavacodeFile.createNewFile();
    this.mCompiledCodeFile = new File( path + ".class" );
    this.mFileWriter = new FileWriter( this.mJavacodeFile );
  }

  //</editor-fold>

  //<editor-fold desc="Methods">

  /**
   * Visit a parse tree produced by {@link code.falx.antlr.brainfuck.grammar.generated.BrainfuckParser#expression}.
   *
   * @param ctx
   *         the parse tree
   *
   * @return the visitor result
   */
  @Override
  public Object visitExpression (
          @NotNull
          ExpressionContext ctx )
  {
    if ( ctx.op != null )
    {
      char op = ctx.op.getText().charAt( 0 );
      this.mLastOp = op;
      this.evalOp( op );
    }
    this.visitChildren( ctx );

    return null;
  }

  /**
   * Visit a parse tree produced by {@link code.falx.antlr.brainfuck.grammar.generated.BrainfuckParser#loop}.
   *
   * @param ctx
   *         the parse tree
   *
   * @return the visitor result
   */
  @Override
  public Object visitLoop (
          @NotNull
          LoopContext ctx )
  {
    StringBuilder sb = new StringBuilder();
    sb.append ( this.writeValueChanges() );
    sb.append( "\t\twhile ( sMemoryMap.get ( sPointer ) != 0 )\n\t\t{\n" );
    this.write( sb.toString() );

    visitChildren( ctx );

    this.write( this.writeValueChanges().toString() );
    this.write( "\t\t}\n" );
    this.mLastOp = ']';

    return null;
  }

  /**
   * Visit a parse tree produced by {@link code.falx.antlr.brainfuck.grammar.generated.BrainfuckParser#program}.
   *
   * @param ctx
   *         the parse tree
   *
   * @return the visitor result
   */
  @Override
  public Object visitProgram (
          @NotNull
          ProgramContext ctx )
  {
    StringBuilder sb = new StringBuilder();
    sb.append( "import java.util.ArrayList;\n\n" );
    sb.append( "class " );
    sb.append( this.filename );
    sb.append( "\n\t{\n" );
    sb.append( "\tprivate static int sPointer = 0;\n\n" );
    sb.append( "\tprivate static ArrayList<Integer> sMemoryMap = new ArrayList<>();\n" );
    sb.append( "\tpublic static void main ( String[] args )\n\t{\n" );
    sb.append( "\t\tsMemoryMap.add( 0 );\n" );
    this.write( sb.toString() );
    sb = new StringBuilder();

    visitChildren( ctx );
    if ( this.mLastOp == '+' || this.mLastOp == '-' )
      sb.append( this.writeValueChanges() );

    sb.append( "\t}\n\n" );

    // define and write increment-method
    sb.append( "\tprivate static void incrementPointer()\n{\n" );
    sb.append( "\tsPointer++;\n" );
    sb.append( "\t\tif ( sPointer == sMemoryMap.size() )\n\t\t{\n" );
    sb.append( "\t\t\tsMemoryMap.add ( 0 );\n" );
    sb.append( "\t\t}\n\t}\n\n" );

    // define and write setValue - method
    sb.append ( "\tprivate static void setValue ( int val )\n\t{\n" );
    sb.append( "\t\tint value = sMemoryMap.get ( " );
    sb.append( "sPointer" );
    sb.append( " );\n" );
    sb.append( "\t\tvalue += val" );
    sb.append( ";\n" );
    sb.append( "\t\tsMemoryMap.set ( " );
    sb.append( "sPointer" );
    sb.append( ", value );\n" );
    sb.append( "\t}\n" );

    sb.append( "}\n" );

    this.write( sb.toString() );

    this.closeFile();
    return null;
  }

  private String buildPath ( String outputPath, String filename )
  {
    String path = outputPath;
    if ( filename.startsWith( File.separator ) )
      filename = filename.substring( 0, filename.length() - 1 );
    if ( !path.endsWith( File.separator ) )
      path += File.separator;
    path = path + filename;
    return path;
  }

  private void closeFile ()
  {
    try
    {
      this.mFileWriter.close();
    }
    catch ( IOException e )
    {
      System.err.println( "An exception occured while closing the file. " +
                                  "See stacktrace below for further information." );
      e.printStackTrace();
      System.exit( -2 );
    }
  }

  private void evalOp ( char op )
  {
    StringBuilder sb = new StringBuilder();
    if ( op == '>' || op == '<' )
    {
      sb.append( this.writeValueChanges() );
      if ( op == '>' )
      {
        this.mCurPointer++;
        sb.append( "\t\tincrementPointer ();\n" );
      }
      else if ( op == '<' )
      {
        this.mCurPointer--;
        sb.append( "\t\tsPointer--;\n" );
        // Compiler check if the current Pointer value is valid -> non negative!
        if ( this.mCurPointer < 0 )
        {
          /*
          TODO:
          is a negative pointer really not allowed as is?
          check if a negative pointer is possible to hold and only its access is
          a cause to exit!
          */
          System.err.println( "Negative pointer! Compiler exited!" );
          System.exit( -3 );
        }
      }
    }
    else if ( op == '+' )
      this.mCurValue++;
    else if ( op == '-' )
      this.mCurValue--;
    else if ( op == '.' )
      sb.append( "\t\tSystem.out.print ( sMemoryMap.get ( sPointer ) );\n" );
    // used if each value-change is compiled
      /*      else if ( op == '+' || op == '-' )
      {
        sb.append( "int val = sMemoryMap.get ( sPointer );\n" );
        if ( op == '+' )
          sb.append( "sMemoryMap.set ( sPointer, ++val );\n" );
        else
          sb.append( "sMemoryMap.set ( sPointer, --val );\n" );
      }*/

    this.write( sb.toString() );
  }

  private void write ( String s )
  {
    try
    {
      this.mFileWriter.write( s );
    }
    catch ( IOException e )
    {
      e.printStackTrace();
      System.exit( -1 );
    }
  }

  private StringBuilder writeValueChanges ()
  {
    StringBuilder sb = new StringBuilder();
    if ( this.mCurValue != 0 )
    {
      sb.append( "\t\tsetValue ( " );
      sb.append( this.mCurValue );
      sb.append( " );\n" );
      this.mCurValue = 0;
    }

    return sb;
  }

  //</editor-fold>
}
