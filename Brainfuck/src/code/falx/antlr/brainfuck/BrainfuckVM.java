package code.falx.antlr.brainfuck;

import java.io.*;
import java.util.ArrayList;

/**
 * @author falx
 * @created 01.01.2015
 */
public class BrainfuckVM
{
  //<editor-fold desc="Fields">

  public static final char sfPOINTER_DECREMENT = '<';
  public static final char sfPOINTER_INCREMENT = '>';
  public static final char sfVALUE_DECREMENT = '-';
  public static final char sfVALUE_INCREMENT = '+';
  public static final char sfWRITE_VALUE = '.';
  public static final char sfREAD_VALUE = ',';
  private InputStreamReader mInReader;
  private ArrayList<Integer> mMemory;
  private OutputStreamWriter mOutWriter;
  private int mPointer;

  //</editor-fold>

  //<editor-fold desc="Constructor">

  public BrainfuckVM ()
  {
    this( System.in, System.out );
  }

  public BrainfuckVM ( InputStream in, OutputStream out )
  {
    this.mOutWriter = new OutputStreamWriter( out );
    this.mInReader = new InputStreamReader( in );
    this.mPointer = 0;
    this.mMemory = new ArrayList<>( 10 );
    this.mMemory.add( 0 );
  }

  //</editor-fold>

  //<editor-fold desc="Methods">

  public void eval ( char operator )
  {
    switch ( operator )
    {
      case ( BrainfuckVM.sfVALUE_INCREMENT ):
        this.incrementValue();
        break;
      case ( BrainfuckVM.sfVALUE_DECREMENT ):
        this.decrementValue();
        break;
      case ( BrainfuckVM.sfPOINTER_INCREMENT ):
        this.incrementPointer();
        break;
      case ( BrainfuckVM.sfPOINTER_DECREMENT ):
        this.decrementPointer();
        break;
      case ( BrainfuckVM.sfREAD_VALUE ):
        this.read();
        break;
      case ( BrainfuckVM.sfWRITE_VALUE ):
        this.write();
        break;
      default:
        throw new IllegalArgumentException( "Invalid operator: " + operator );
    }
  }

  public int getValue ( int i )
  {
    return this.mMemory.get( i );
  }

  public boolean loopToDo ()
  {
    return this.mMemory.get( this.mPointer ) != 0;
  }

  private void decrementPointer ()
  {
    this.mPointer--;
  }

  private void decrementValue ()
  {
    int value = this.mMemory.get( this.mPointer );
    this.mMemory.set( this.mPointer, --value );
  }

  private void incrementPointer ()
  {
    this.mPointer++;
    if ( this.mMemory.size() == this.mPointer )
      this.mMemory.add( 0 );
  }

  private void incrementValue ()
  {
    int value = this.mMemory.get( this.mPointer );
    this.mMemory.set( this.mPointer, ++value );
  }

  private void read ()
  {
    try
    {
      int i = this.mInReader.read();
      this.mMemory.set( this.mPointer, i );
    }
    catch ( IOException e )
    {
      e.printStackTrace();
      System.exit( -1 );
    }
  }

  private void write ()
  {
    try
    {
      int i = this.mMemory.get( this.mPointer );
      this.mOutWriter.write( i );
    }
    catch ( IOException e )
    {
      e.printStackTrace();
      System.exit( -2 );
    }
  }
  //</editor-fold>
}
