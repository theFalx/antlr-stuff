package code.falx.antlr.brainfuck;

import code.falx.antlr.brainfuck.grammar.generated.BrainfuckBaseVisitor;
import code.falx.antlr.brainfuck.grammar.generated.BrainfuckParser;
import org.antlr.v4.runtime.misc.NotNull;

/**
 * @author falx
 * @created 02.01.2015
 */
public class BrainfuckInterpreter <T>
        extends BrainfuckBaseVisitor<Object>
{
  //<editor-fold desc="Fields">

  private BrainfuckVM mBrainfuckVM;

  //</editor-fold>

  //<editor-fold desc="Constructor">

  public BrainfuckInterpreter ()
  {
    this.mBrainfuckVM = new BrainfuckVM ();
  }

  //</editor-fold>

  //<editor-fold desc="Methods">

  /**
   * {@inheritDoc}
   * <p/>
   * <p>The default implementation returns the result of calling
   * {@link #visitChildren} on {@code ctx}.</p>
   */
  @Override
  public Object visitExpression (
          @NotNull
          BrainfuckParser.ExpressionContext ctx )
  {
    if ( ctx.op != null )
      this.mBrainfuckVM.eval( ctx.op.getText().charAt( 0 ) );
    return visitChildren( ctx );
  }

  /**
   * {@inheritDoc}
   * <p/>
   * <p>The default implementation returns the result of calling
   * {@link #visitChildren} on {@code ctx}.</p>
   */
  @Override
  public Object visitLoop (
          @NotNull
          BrainfuckParser.LoopContext ctx )
  {
    while ( this.mBrainfuckVM.loopToDo() )
      super.visitChildren( ctx );
    return null;
  }

  /**
   * {@inheritDoc}
   * <p/>
   * <p>The default implementation returns the result of calling
   * {@link #visitChildren} on {@code ctx}.</p>
   */
  @Override
  public Object visitProgram (
          @NotNull
          BrainfuckParser.ProgramContext ctx )
  {
    return visitChildren( ctx );
  }

  public BrainfuckVM getBrainfuckVM ()
  {
    return this.mBrainfuckVM;
  }

  //</editor-fold>
}
