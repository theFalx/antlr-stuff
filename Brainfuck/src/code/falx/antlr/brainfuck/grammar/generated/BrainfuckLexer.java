// Generated from C:/Users/Kristoffer/repositories/antlr-stuff/Brainfuck/src/code/falx/antlr/brainfuck/grammar\Brainfuck.g4 by ANTLR 4.4.1-dev

package code.falx.antlr.brainfuck.grammar.generated;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BrainfuckLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.4.1-dev", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		OPERATIONS=1, WHILE_START=2, WHILE_END=3, WS=4;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'"
	};
	public static final String[] ruleNames = {
		"VALUE_INCREMENT", "VALUE_DECREMENT", "POINTER_INCREMENT", "POINTER_DECREEMNT", 
		"READ_STDIN", "WRITE_STDOUT", "OPERATIONS", "WHILE_START", "WHILE_END", 
		"WS"
	};


	public BrainfuckLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Brainfuck.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\6\63\b\1\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\5\b*\n\b\3\t\3\t\3\n\3\n\3\13\3\13\3\13\3\13\2\2\f\3\2\5\2\7"+
		"\2\t\2\13\2\r\2\17\3\21\4\23\5\25\6\3\2\3\5\2\13\f\17\17\"\"\61\2\17\3"+
		"\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\3\27\3\2\2\2\5\31\3\2\2"+
		"\2\7\33\3\2\2\2\t\35\3\2\2\2\13\37\3\2\2\2\r!\3\2\2\2\17)\3\2\2\2\21+"+
		"\3\2\2\2\23-\3\2\2\2\25/\3\2\2\2\27\30\7-\2\2\30\4\3\2\2\2\31\32\7/\2"+
		"\2\32\6\3\2\2\2\33\34\7@\2\2\34\b\3\2\2\2\35\36\7>\2\2\36\n\3\2\2\2\37"+
		" \7.\2\2 \f\3\2\2\2!\"\7\60\2\2\"\16\3\2\2\2#*\5\3\2\2$*\5\5\3\2%*\5\7"+
		"\4\2&*\5\t\5\2\'*\5\13\6\2(*\5\r\7\2)#\3\2\2\2)$\3\2\2\2)%\3\2\2\2)&\3"+
		"\2\2\2)\'\3\2\2\2)(\3\2\2\2*\20\3\2\2\2+,\7]\2\2,\22\3\2\2\2-.\7_\2\2"+
		".\24\3\2\2\2/\60\t\2\2\2\60\61\3\2\2\2\61\62\b\13\2\2\62\26\3\2\2\2\4"+
		"\2)\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}