// Generated from C:/Users/Kristoffer/repositories/antlr-stuff/Brainfuck/src/code/falx/antlr/brainfuck/grammar\Brainfuck.g4 by ANTLR 4.4.1-dev

package code.falx.antlr.brainfuck.grammar.generated;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BrainfuckParser}.
 */
public interface BrainfuckListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link BrainfuckParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(@NotNull BrainfuckParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BrainfuckParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(@NotNull BrainfuckParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BrainfuckParser#loop}.
	 * @param ctx the parse tree
	 */
	void enterLoop(@NotNull BrainfuckParser.LoopContext ctx);
	/**
	 * Exit a parse tree produced by {@link BrainfuckParser#loop}.
	 * @param ctx the parse tree
	 */
	void exitLoop(@NotNull BrainfuckParser.LoopContext ctx);
	/**
	 * Enter a parse tree produced by {@link BrainfuckParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(@NotNull BrainfuckParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link BrainfuckParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(@NotNull BrainfuckParser.ProgramContext ctx);
}