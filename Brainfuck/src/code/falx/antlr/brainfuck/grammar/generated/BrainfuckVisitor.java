// Generated from C:/Users/Kristoffer/repositories/antlr-stuff/Brainfuck/src/code/falx/antlr/brainfuck/grammar\Brainfuck.g4 by ANTLR 4.4.1-dev

package code.falx.antlr.brainfuck.grammar.generated;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link BrainfuckParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface BrainfuckVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link BrainfuckParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(@NotNull BrainfuckParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link BrainfuckParser#loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop(@NotNull BrainfuckParser.LoopContext ctx);
	/**
	 * Visit a parse tree produced by {@link BrainfuckParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(@NotNull BrainfuckParser.ProgramContext ctx);
}