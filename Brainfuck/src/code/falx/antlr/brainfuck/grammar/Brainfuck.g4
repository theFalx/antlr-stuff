grammar Brainfuck;

@parser::header
{
package code.falx.antlr.brainfuck.grammar.generated;
}

@parser::member
{

}

@lexer::header
{
package code.falx.antlr.brainfuck.grammar.generated;
}

@lexer::member
{

}


fragment VALUE_INCREMENT : '+';
fragment VALUE_DECREMENT : '-';

fragment POINTER_INCREMENT : '>';
fragment POINTER_DECREEMNT : '<';

fragment READ_STDIN : ',';
fragment WRITE_STDOUT: '.';

OPERATIONS :  ( VALUE_INCREMENT | VALUE_DECREMENT | POINTER_INCREMENT | POINTER_DECREEMNT | READ_STDIN | WRITE_STDOUT );

WHILE_START : '[';
WHILE_END : ']';

WS : ( ' ' | '\t' | '\n' | '\r'  ) -> skip;

program : expression EOF
        ;

expression : op=OPERATIONS expression
           | loop expression
           | op=OPERATIONS?
           ;

loop : WHILE_START expression WHILE_END
     ;